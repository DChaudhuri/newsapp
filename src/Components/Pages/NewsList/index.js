import React from 'react';
import NewsArticle from '../NewsArticle';
import styles from './NewsList.module.css';
const NewsList=({newslist})=>{
    return(
        <div className={styles.container}>
            {
                newslist.map((item,index)=><NewsArticle {...item} />)
            }
        </div>
    )
}
export default NewsList;