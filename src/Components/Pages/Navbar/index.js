import React, { useState } from 'react';
import styles from './Navbar.module.css';
const Navbar=({search})=>{
    const [text,setText]=useState('')
    const change=(event)=>{
        setText(event.target.value)
    }
    const click=()=>{
        search(text);
        setText('')
    }
    return(
        <div className={styles.container}>
            <div className={styles.header}>
                <h2>News</h2>
            </div>
            <div className={styles.search}>
                <input placeholder="Search for Article" onChange={change} value={text} />
                <button onClick={click} >Search</button>
            </div>
        </div>
    )
}
export default Navbar;