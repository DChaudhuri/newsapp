import React from 'react';
import styles from './CountryList.module.css';
const countryNames=['India','Argentina','Brazil','Belgium','Spain','Italy','Turkey','Japan','China','Hong Kong','Netherlands','Greece','Canada','Mexico','Germany','Australia','Austria','Peru','Ukraine','Iceland','Ireland','Egypt','Cuba','Colombia','Portugal']
const CountryList=({countries})=>{
    const click=(item)=>{
        countries(item)
    }
    return(
        <div className={styles.container}>
            <p className={styles.titleText}>Country </p>
            <div className={styles.lists}>
                {
                    countryNames.map((item)=><button onClick={()=>click(item)}>{item}</button>)
                }
            </div>
        </div>
    )
}
export default CountryList;