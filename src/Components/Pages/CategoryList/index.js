import React from 'react';
import styles from "./CategoryList.module.css";
const categoryList=['Business','Technology','Entertainment','Sports','Health','Science']
const CategoryList=({categories})=>{
    const click=(item)=>{
        categories(item)
    }
    return(
        <div className={styles.container}>
            <ul>Category :</ul>
            <div className={styles.lists}>
                {
                    categoryList.map(
                    (item)=><ul><button onClick={()=>click(item)}>{item}</button></ul>
                    )
                }
            </div>
        </div>
    )
}
export default CategoryList;