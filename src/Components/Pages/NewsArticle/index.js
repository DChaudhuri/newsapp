import React, { useState } from 'react';
import styles from './NewsArticle.module.css'
const NewsArticle=(props)=>{
    const [readmore,setReadMore]=useState(false)
    return(
        <div className={styles.container}>
            <div className={styles.imageContainer}>
                <div className={styles.imageBox}>
                    <img alt="image" src={props.urlToImage} />
                </div>
            </div>
            <div className={styles.titleAndDescription}>
                <span className={styles.titleText}>{props.title}</span>
                <div className={styles.source}>
                    <div>
                        <span>{props.source.name}&nbsp;&nbsp;</span>
                        <i class="fas fa-share-alt color:red"   ></i>
                    </div>
                    
                    <button className={`${readmore && styles.buttonAnimation }`} onClick={()=>setReadMore(!readmore)} ><i  class="fa fa-chevron-down fa-lg icon" aria-hidden="false" ></i></button>
                </div>
                <div className={`${styles.description} ${readmore ? styles.expand : styles.collapse}`}>
                    <p >Description :</p>
                    <span className={styles.descriptionText}>{props.description}</span>
                    <span className={styles.knowMore}>&nbsp;...more</span>
                </div>
            </div>
        </div>
    )
}
export default NewsArticle;