import React, { useEffect, useState } from 'react';
import Navbar from './Pages/Navbar/index';
import Home from './Home';
import { getCode } from 'country-list';
const RootPage=()=>{
    const [newslist,setList]=useState([])
    useEffect(()=>{
        fetch(`https://newsapi.org/v2/top-headlines?country=in&apiKey=a9b320f1f47644f99dab6b9407db90b0`).then((response)=>{
         return response.json();
        }).then((json)=>{
            setList(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    },[])
    const search=(query)=>{
        if(query)
        fetch(`https://newsapi.org/v2/everything?q=${query}&apiKey=a9b320f1f47644f99dab6b9407db90b0`).then((response)=>{
         return response.json();
        }).then((json)=>{
            setList(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
        else
        alert('Empty search field')
    }
    const categories=(query)=>{
        fetch(`https://newsapi.org/v2/everything?q=${query}&sortBy=popularity&apiKey=a9b320f1f47644f99dab6b9407db90b0`).then((response)=>{
         return response.json();
        }).then((json)=>{
            setList(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    }
    const countries=(query)=>{
        let code=getCode(query)
        console.log(code)
        fetch(`https://newsapi.org/v2/top-headlines?country=${code}&sortBy=popularity&apiKey=a9b320f1f47644f99dab6b9407db90b0`).then((response)=>{
         return response.json();
        }).then((json)=>{
            setList(json.articles)
        }).catch(()=>{
            console.log("Error in fetch")
        })
    }
    return(
        <>
            <Navbar  search={search} />
            <Home newslist={newslist} categories={categories} countries={countries} />
        </>
    )
}
export default RootPage;