import React from 'react';
import NewsList from './Pages/NewsList';
import CategoryList from './Pages/CategoryList';
import CountryList from './Pages/CountryList';
import styles from "./Home.module.css";
const Home=({newslist,categories,countries })=>{
    return(
        <div className={styles.container}>
            <CategoryList categories={categories} />
            <NewsList newslist={newslist} />
            <CountryList countries={countries} />
        </div>
    )
}
export default Home;