import React from 'react';
import RootPage from './Components/RootPage';
import './App.css';

function App() {
  return (
    <RootPage />
  );
}

export default App;
